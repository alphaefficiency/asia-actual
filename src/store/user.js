import Vue from 'vue'
import Vuex from 'vuex'
import router from '@/router/';
// import firebase from '@/store/firebase';
import { firebase } from '@firebase/app';
// import * as isUserLoggedIn from '@/store/isUserLoggedIn';
// import { axiosClient } from '../axios-client';
require('firebase/auth')

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    isLoading: false,
  },
  mutations: {
    resultsAreLoading(state) {
      state.isLoading = true;
    },

    resultsLoaded(state) {
      state.isLoading = false;
    }
  },
  actions: {
    async login({ dispatch }, form) {
      let errsFrame = document.querySelector('span.login-errors');

      firebase.auth().setPersistence(firebase.auth.Auth.Persistence.SESSION)
        .then(() => firebase.auth().signInWithEmailAndPassword(form.email, form.password)).then(()=>router.push('/search'))
        .catch((err) => {
          switch(err.code) {
                default: {
                  errsFrame.innerHTML = 'Something went wrong.';
                  errsFrame.style.visibility = 'initial';
                  break;
                }
      
                case 'auth/wrong-password': {
                  errsFrame.innerHTML = 'Password is not correct.';
                  errsFrame.style.visibility = 'initial';
                  break;
                }
      
                case 'auth/invalid-email': {
                  errsFrame.innerHTML = 'Email is not in correct format.';
                  errsFrame.style.visibility = 'initial';
                  break;
                }
              }
        })

      // sign user in
      // try {
      //   const user = await firebase.auth.signInWithEmailAndPassword(form.email, form.password);

      //   if(isUserLoggedIn.isUserLoggedIn()) {
      //     errsFrame.style.visibility = 'hidden';
      //     router.push('/search')
      //   }
  
      // } catch(ex) {
      //   switch(ex.code) {
      //     default: {
      //       errsFrame.innerHTML = 'Something went wrong.';
      //       errsFrame.style.visibility = 'initial';
      //       break;
      //     }

      //     case 'auth/wrong-password': {
      //       errsFrame.innerHTML = 'Password is not correct.';
      //       errsFrame.style.visibility = 'initial';
      //       break;
      //     }

      //     case 'auth/invalid-email': {
      //       errsFrame.innerHTML = 'Email is not in correct format.';
      //       errsFrame.style.visibility = 'initial';
      //       break;
      //     }
      //   }
      // }
    },

    async forgotPassword({dispatch}, email) {
      let errsFrame = document.querySelector('span.errors');

      try {
        firebase.auth().sendPasswordResetEmail(email.email)
          .then(() => {
            this.commit('resultsLoaded')
            errsFrame.style.visibility = 'initial';
            errsFrame.innerHTML = 'Please check your e-mail inbox.';
            setTimeout(function() {router.push('/search')}, 5000);
          });
      } catch (ex) {
        errsFrame.innerHTML = 'Something went wrong with reseting your password.';
        errsFrame.style.visibility = 'initial';
      }
    },

    // async signup({ dispatch }, form) {
    //   let errsFrame = document.querySelector('span.register-errors');
    //   // sign user up
    //   try {
    //     const user = await firebase.auth.createUserWithEmailAndPassword(form.email, form.password)

    //     axiosClient.post('v1/account', {
    //       tier: "FREE"
    //     });

    //     if(isUserLoggedIn.isUserLoggedIn()) {
    //       router.push('/search')
    //     }

    //   } catch (ex) {
    //     switch(ex.code) {
    //       default: {
    //         errsFrame.innerHTML = 'Something went wrong.';
    //         errsFrame.style.visibility = 'initial';
    //         break;
    //       }

    //       case 'auth/weak-password': {
    //         errsFrame.innerHTML = 'Password should be longer than 6 characters.';
    //         errsFrame.style.visibility = 'initial';
    //         break;
    //       }

    //       case 'auth/invalid-email': {
    //         errsFrame.innerHTML = 'Email is not in correct format.';
    //         errsFrame.style.visibility = 'initial';
    //         break;
    //       }
        
    //     }
    //   }
    
    // },

    // async fetchUserProfile({ commit }, user) {
    //   // fetch user profile
    //   const userProfile = firebase.auth.currentUser;

    //   console.warn(userProfile);

    //   // set user profile in state
    //   commit('setUserProfile', userProfile)
      
    //   // change route to dashboard
    //   router.push('/search')
    // },
  }
})