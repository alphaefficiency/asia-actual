import {auth} from './firebase'

let user;

auth.onAuthStateChanged((updatedUser) => user = updatedUser)

export function isUserLoggedIn() {
  if(!user) return false;

  return true;
}
