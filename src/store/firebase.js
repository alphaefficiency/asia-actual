import { firebase } from '@firebase/app';
import 'firebase/firestore';
require('firebase/auth');

// firebase init - add your own config here
// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
// TODO: We should move these into environment variables.
var firebaseConfig = {
  apiKey: "AIzaSyC4LJJHWNIeT_SobifW0A0kbHN1EapKC5o",
  authDomain: "firm-star-295213.firebaseapp.com",
  projectId: "firm-star-295213",
  storageBucket: "firm-star-295213.appspot.com",
  messagingSenderId: "290162916236",
  appId: "1:290162916236:web:e0492342f61139abd974a9",
  measurementId: "G-60BRX22SKT"
};

const firebaseApp = firebase.initializeApp(firebaseConfig);

export const db = firebaseApp.firestore();
export const auth = firebase.auth();

// export utils/refs
export default {
  db,
  auth
}