import axios from 'axios';
import firebase from './store/firebase'

const ROSE_BASE_URL = 'https://aad-backend-dot-firm-star-295213.uc.r.appspot.com'

export const axiosClient = axios.create({
    baseURL: ROSE_BASE_URL
});

axiosClient.interceptors.request.use(async (config) => {
    const user = firebase.auth.currentUser
    if(!user) return config;
    const token = await user.getIdToken(true)
    config.headers.Authorization = token;

    return config;
})