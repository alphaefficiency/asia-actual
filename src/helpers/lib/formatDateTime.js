/*
	Format date and time to MM/DD/YYYY - hh:mm:ss
	
	@param {String} inputDate - Date input.
	Input date-time format should be ISO-8601 in UTC - https://en.wikipedia.org/wiki/ISO_8601
	Example: 2020-01-17T13:00:00.000Z
*/
export default (inputDate) => {
	if (!inputDate) {
		return
	}
	const time = inputDate.split('T')[1].split('.')[0]

	// Date
	const date = new Date(inputDate)
	const month = date.getMonth() > 8 ? date.getMonth() + 1 : '0' + (date.getMonth() + 1)
	const day = date.getDate() > 9 ? date.getDate() : ('0' + date.getDate())
	const year = date.getFullYear()
	const finalDate = month + '/' + day + '/' + year

	// const formatedDate = {
	// 	date: finalDate,
	// 	time: time
	// }

	return `${finalDate} - ${time}`
}