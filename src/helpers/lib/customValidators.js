/*
	Checks if a string contains ONLY alphabetic characters and whitespaces
	
	@param {String} string - String/sentence to check.
	Valid examples: 'This is test string one', 'test'
	Invalid examples: 'This is test string one.', 'This is test string 1'
*/

export function onlyAlphaChars(string) {

	// Check if there is at least one character in a string
	if (string.length > 0) {
		const trimmedString = string.trim();
		const onlyAlphabeticCharacters = /^[a-zA-Z ]+$/i;

		return onlyAlphabeticCharacters.test(trimmedString);

	// If there are no characters in a string - don't validate it
	} else {
		return true;
	}
}