
const devUrl = process.env.VUE_APP_API_DEV_URL || 'http://localhost:1334'
const prodUrl = process.env.VUE_APP_API_PROD_URL || '?' //? | http://localhost:1334
const baseApiUrl = process.env.NODE_ENV === 'development' ? devUrl : prodUrl

module.exports = {
	NODE_ENV: process.env.NODE_ENV || 'development',
	DEV_URL: devUrl,
	PROD_URL: prodUrl,
	BASE_API_URL: baseApiUrl,
	// Enable/disable console logging - reload the page after changing:
	LOGS_ENABLED: true,
	ADMIN_AUTH: true,		// true or false
	USER_AUTH: true,		// true or false
	devServer: { https: true }
}