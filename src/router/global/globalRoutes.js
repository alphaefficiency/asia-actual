/*
	Global routes - don't need to be authenticated/logged in to get this assets
*/


const routes = [
	{
		path: '/',
		name: 'index',
		meta: {
			title: 'Home',
			requiresAuth: false
		},
		component: () => import('@/views/global/index.vue')
	},
	{
		path: '/search',
		name: 'search',
		meta: {
			title: 'SearchPage',
			requiresAuth: false
		},
		component: () => import('@/views/global/search.vue')
	},
	{
		path: '/account',
		name: 'account',
		meta: {
			title: 'Account',
			requiresAuth: false
		},
		component: () => import('@/views/user/dashboard/account.vue')
	},
	{
		path: '/saved',
		name: 'saved',
		meta: {
			title: 'Saved Searches',
			requiresAuth: false
		},
		component: () => import('@/views/user/dashboard/saved-searches.vue')
	},
	{
		path: '/pricing-plan',
		name: 'pricing-plan',
		meta: {
			title: 'Pricing Plan',
			requiresAuth: false
		},
		component: () => import('@/views/global/pricing-plan.vue')
	},
	{
		path: '/expiry-alerts',
		name: 'expiry-alerts',
		meta: {
			title: 'Expiry Alerts',
			requiresAuth: false
		},
		component: () => import('@/views/user/dashboard/expiry-alerts.vue')
	},
	{
		path: '/login',
		name: 'login',
		meta: {
			title: 'Login',
			requiresAuth: false
		},
		component: () => import('@/views/global/login.vue')
	},
	{
		path: '/register',
		name: 'register',
		meta: {
			title: 'Register',
			requiresAuth: false
		},
		component: () => import('@/views/global/register.vue')
	},
	{
		path: '/thank-you',
		name: 'thank-you',
		meta: {
			title: 'Thank You Page',
			requiresAuth: false
		},
		component: () => import('@/views/global/thank-you.vue')
	},
	{
		path: '/cancel-renewal',
		name: 'cancel',
		meta: {
			title: 'Subscription Canceled',
			requiresAuth: false
		},
		component: () => import('@/views/global/cancel.vue')
	},
	{
		path: '/checkout',
		name: 'checkout',
		meta: {
			title: 'Home',
			requiresAuth: false
		},
		component: () => import('@/views/stripe/checkout.vue')
	},
	{
		path: '/stripe',
		name: 'stripe',
		meta: {
			title: 'Home',
			requiresAuth: false
		},
		component: () => import('@/views/stripe/stripeForm.vue')
	},
	{
		path: '/billing-info',
		name: 'billing-info',
		meta: {
			title: 'Billing Information',
			requiresAuth: false
		},
		component: () => import('@/views/global/billing-info.vue')
	},
	{
		path: '/reset-password',
		name: 'reset-password',
		meta: {
			title: 'Reset Password',
			requiresAuth: false
		},
		component: () => import('@/views/global/forgot-pass.vue')
	},
	{
		path: '/upgrade',
		name: 'upgrade-to-premium',
		meta: {
			title: 'Search',
			requiresAuth: false
		},
		component: () => import('@/views/global/mockup.vue')
	},
	{
		path: '/404',
		name: 'notFound',
		meta: {
			title: 'Not found',
			requiresAuth: false
		},
		component: () => import('@/views/global/404.vue')
	},

	// "Catch all route" - Needs to be the last route (redirects on 404 page)
	{
		path: '*',
		redirect: '/404',
		meta: {
			requiresAuth: false
		}
	}
]

export default routes