/*
  User routes - need to be authenticated as a user to get this assets
*/
// Config
import config from '@/config/config'

// Should be always be bool - false - or string 'user'
let isUserAuthRequired = config.USER_AUTH === true ? 'user' : false
if (process.env.NODE_ENV === 'production') isUserAuthRequired = 'user'

// User Dashboard
import UserDashboard from '@/components/user/dashboard/UserDashboard'
/* User Dashboard Pages
====================================== */
// User Dashboard pages
import UserDashboardAccount from '@/views/user/dashboard/account'

const routes = [

	{
		path: '/return',
	},
	{
		// *import* generates a separate chunk (about.[hash].js) for this route
		// which is lazy-loaded when the route is visited
		// for improving performance
		path: '/user',
		redirect: '/user/dashboard',
		component: UserDashboard,
		meta: { requiresAuth: isUserAuthRequired },

		// Sub routes for User Dashboard
		children: [
			{
				path: '/my-account',
				name: 'user-account',
				component: UserDashboardAccount,
				meta: {
					title: 'Account',
					requiresAuth: isUserAuthRequired,
					accountPage: true
				}
			},
			
			{
				path: '/user/dashboard',
				name: 'user-dashboard',
				meta: {
					title: 'Dashboard',
					requiresAuth: false
				},
				component: () => import('@/views/user/dashboard/dashboard.vue'),
			},
		]
	},
	{
		path: '*',
		redirect: '/404'
	}
]

export default routes