import Vue from 'vue';
import Router from 'vue-router';
import globalRoutes from '@/router/global/globalRoutes';
import userRoutes from '@/router/user/userRoutes';

Vue.use(Router);

var allRoutes = []
allRoutes = allRoutes.concat(globalRoutes, userRoutes);

const routes = allRoutes

export default new Router({
	mode: 'history',
	// TODO: Check if web server is configured to handle accessing routes
	// other than / (index) route
	base: process.env.BASE_URL || '/',
	routes: routes
});