//important tips are on the bottom
import Vue from 'vue'
import App from './App.vue'
import router from './router/index'
import store from '@/store/user'
import { firebase } from '@firebase/app';
import * as isUserLoggedIn from '@/store/isUserLoggedIn';
import './plugins'
import "@/assets/css/global.scss"
Vue.config.productionTip = false

let app
firebase.auth().onAuthStateChanged(function(user) {
  if (!app) {
  	app = new Vue({
			router,
			store,
			render: h => h(App)
		}).$mount('#app')
	}
});

//Make sure to only ever call next() one time or you could run into errors or incorrect path resolution!