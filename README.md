# Asia Actualt Client

## [Live Deployment](https://firm-star-295213.uc.r.appspot.com/)

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run start
```

### Compiles and minifies for production
```
npm run build:production
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
